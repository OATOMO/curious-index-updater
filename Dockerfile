FROM python:2.7-alpine
WORKDIR /usr/src/app
COPY . .
RUN \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev libxml2-dev libxslt-dev && \
 apk add --no-cache coreutils  postgresql-libs  libxml2 libxslt && \
 python2 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps
