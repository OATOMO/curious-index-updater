# -*- coding: utf-8 -*-#
# -------------------------------------------------------------------------------
# Name:         main.py
# Description:  
# Date:         2020/12/17
# -----------------------------------------------------------------------------
# encoding=utf8
import pymongo
import kafka
from kafka import KafkaProducer
import arrow
import json
import argparse
import requests
import sys
import copy
import psycopg2
import time
import  decimal
import urllib

reload(sys)
sys.setdefaultencoding('utf-8')

cityLists = [
['保山市','保山','保山市','379'],
['楚雄彝族自治州','楚雄','楚雄州','380'],
['大理白族自治州','大理','大理州','381'],
['德宏傣族景颇族自治州','德宏','德宏州','382'],
['迪庆藏族自治州','迪庆','迪庆州','383'],
['红河哈尼族彝族自治州','红河','红河州','384'],
['昆明市','昆明','昆明市','385'],
['临沧市','临沧','临沧市','387'],
['丽江市','丽江','丽江市','386'],
['怒江傈僳族自治州','怒江','怒江州','388'],
['普洱市','普洱','普洱市','389'],
['文山壮族苗族自治州','文山','文山州','391'],
['曲靖市','曲靖','曲靖市','390'],
['玉溪市','玉溪','玉溪市','393'],
['西双版纳傣族自治州','西双版纳','西双版纳州','392'],
['昭通市','昭通','昭通市','394'],
]

cityDict = {
    '379': '保山市', '380': '楚雄州', '381': '大理州', '382': '德宏州', '383': '迪庆州', '384': '红河州', '385': '昆明市', '387': '临沧市',
    '386': '丽江市', '388': '怒江州', '389': '普洱市', '391': '文山州', '390': '曲靖市', '393': '玉溪市', '392': '西双版纳州', '394': '昭通市',
    '3007': '昌宁县', '3009': '隆阳区', '3008': '龙陵县', '3011': '腾冲市', '3010': '施甸县', '3012': '楚雄市', '3013': '大姚县',
    '3014': '禄丰县', '3015': '牟定县', '3017': '双柏县', '3016': '南华县', '3019': '姚安县', '3018': '武定县', '3021': '元谋县',
    '3020': '永仁县', '3023': '大理市', '3022': '宾川县', '3025': '鹤庆县', '3024': '洱源县', '3026': '剑川县', '3027': '弥渡县',
    '3028': '南涧县', '3029': '巍山县', '3030': '祥云县', '3031': '漾濞县', '3032': '永平县', '3033': '云龙县', '3034': '瑞丽市',
    '3037': '盈江县', '3036': '梁河县', '3038': '陇川县', '3040': '维西县', '3039': '德钦县', '3041': '香格里拉市', '3043': '河口县',
    '3042': '个旧市', '3045': '建水县', '3044': '红河县', '3046': '金平县', '3047': '开远市', '3048': '泸西县', '3050': '蒙自市',
    '3049': '绿春县', '3051': '弥勒市', '3052': '屏边县', '3053': '石屏县', '3054': '元阳县', '3056': '呈贡区', '3055': '安宁市',
    '3058': '富民县', '3057': '东川区', '3059': '官渡区', '3060': '晋宁区', '3062': '盘龙区', '3061': '禄劝县', '3064': '嵩明县',
    '3063': '石林县', '3066': '西山区', '3065': '五华区', '3067': '寻甸县', '3068': '宜良县', '3070': '华坪县', '3069': '古城区',
    '3072': '永胜县', '3071': '宁蒗县', '3073': '玉龙县', '3074': '沧源县', '3076': '耿马县', '3075': '凤庆县', '3077': '临翔区',
    '3079': '永德县', '3078': '双江县', '3081': '镇康县', '3080': '云县', '3082': '泸水市', '3083': '福贡县', '3084': '贡山县',
    '3085': '兰坪县', '3087': '景东县', '3086': '江城县', '3089': '澜沧县', '3088': '景谷县', '3090': '孟连县', '3091': '墨江县',
    '3093': '思茅区', '3092': '宁洱县', '3094': '西盟县', '3095': '镇沅县', '3096': '富源县', '3097': '会泽县', '3098': '陆良县',
    '3099': '罗平县', '3100': '马龙区', '3101': '麒麟区', '3102': '师宗县', '3104': '沾益区', '3103': '宣威市', '3105': '富宁县',
    '3106': '广南县', '3108': '马关县', '3107': '麻栗坡县', '3113': '景洪市', '3110': '文山市', '3114': '勐海县', '3115': '勐腊县',
    '3116': '澄江市', '3118': '红塔区', '3117': '峨山县', '3119': '华宁县', '3120': '江川区', '3121': '通海县', '3122': '新平县',
    '3124': '元江县', '3123': '易门县', '3125': '昭阳区', '3126': '鲁甸县', '3127': '巧家县', '3128': '盐津县', '3129': '大关县',
    '3130': '永善县', '3131': '绥江县', '3132': '镇雄县', '3133': '彝良县', '3134': '威信县', '3135': '水富市', '3340': '芒市',
    '3346': '西畴县', '3345': '丘北县', '3347': '砚山县'

}

catDict = {
	"27":{"name":"文创商品"},
	"28":{"name":"手工艺品"},
	"29":{"name":"传统茗茶"},
	"30":{"name":"鲜花水果"},
	"31":{"name":"特产食材"},
	"32":{"name":"特色零食 "},
	"33":{"name":"冲调饮品"},

	"34":{"name":"臻藏普洱","par":"传统茗茶"},
	"35":{"name":"生茶","par":"传统茗茶"},
	"36":{"name":"熟茶","par":"传统茗茶"},
	"56":{"name":"红茶","par":"传统茗茶"},
	"37":{"name":"其他茶类","par":"传统茗茶"},

	"53":{"name":"鲜花","par":"鲜花水果"},
	"54":{"name":"水果","par":"鲜花水果"},

	"46":{"name":"山珍","par":"特产食材"},
	"45":{"name":"腊味","par":"特产食材"},
	"47":{"name":"粮油","par":"特产食材"},
	"48":{"name":"调料","par":"特产食材"},

	"49":{"name":"鲜花饼","par":"特色零食"},
	"50":{"name":"坚果","par":"特色零食"},
	"51":{"name":"糖果","par":"特色零食"},
	"52":{"name":"休闲美食","par":"特色零食"},

	"38":{"name":"咖啡","par":"冲调饮品"},
	"41":{"name":"美酒","par":"冲调饮品"},
	"42":{"name":"蜂蜜","par":"冲调饮品"},
	"43":{"name":"天然冲调","par":"冲调饮品"},
	"44":{"name":"饮料","par":"冲调饮品"},
}


def findMixRate(rate_col, hotelId):
    rates = rate_col.find({'hotel_id': hotelId, 'status': True}, {'rate': 1}).limit(1).sort(
        [("rate", pymongo.ASCENDING)])
    mixRate = 0
    for rate in rates:
        mixRate = rate['rate']
    return mixRate


def updateRate(kafkaPro, id, rate):
    body = {
        "Action": "update",
        "DocumentID": id,
        "Index": "alias_cn_yyn_hotel",
        "body": {
            "price": int(rate),
            "_indexTime": arrow.now('Asia/Chongqing').timestamp
        }
    }
    kafkaPro.send('curious-index', json.dumps(body).encode('utf-8'))
    # print '{0}: {1}, send'.format(id, rate)


def hotelPro(kafkaHost):
    producer = KafkaProducer(bootstrap_servers=kafkaHost)
    conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    db = conn['ti_hotel']
    rate_col = db['rate']
    detail_col = db['detail']

    hotelIds = detail_col.find({'status': True, "credit.grade": {"$gt": 0}}, {'hotel_id': 1})
    ids = []
    for id in hotelIds:
        if 'hotel_id' in id.keys():
            ids.append(id['hotel_id'])
    for id in ids:
        mixRate = 0
        mixRate = findMixRate(rate_col, id)
        if mixRate != 0:
            updateRate(producer, id, mixRate)
    producer.flush()
    producer.close()
    print 'hotel update price num: {0}, send'.format(len(ids))


def _getSpot(city_id, context):
    url = 'https://gw.ybsjyyn.com/ticket/api/api/get_list'
    params = {
        'size': 10,
        'context': context,
        'city_id': city_id
    }
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }
    res = requests.get(url, params=params, headers=headers)
    res.encoding = 'utf-8'
    print res.text
    return res.text


def getSpot():
    ticketList = []
    for city_id in cityDict.keys():
        context = _getSpot(city_id, '')
        data = json.loads(context)
        if data['msg'] != 'OK':
            return
        spot_list = data['data']['spot_list']
        ticketList += spot_list
    # print 'ticket num : {0}'.format(len(ticketList))
    return ticketList


def _getTicket(spot_id):
    ticketIdList = []
    url = 'http://gw.ybsjyyn.com/ticket/api/api/detail'
    params = {
        'spot_id': spot_id,
    }
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }
    res = requests.get(url, params=params, headers=headers)
    res.encoding = 'utf-8'
    return res.text


def _getTicket2():
    ticketIdList = []
    url = 'http://gw.ybsjyyn.com/ticket/api/api/detail'
    params = {
        'spot_id': "b569ed5c-ccc7-4694-6ec0-7a3befa15377",
    }
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }
    res = requests.get(url, params=params, headers=headers)
    res.encoding = 'utf-8'
    return res.text

def getTicket2():
    rows = []
    for spot in ['1']:
        context = _getTicket2()
        data = json.loads(context)
        print data
        row = {}
        if data['msg'] != 'OK':
            return
        row['category'] = 'ticket'
        row['categoryDisplay'] = '门票'
        row['thumbUrl'] = data['data']['img_url']
        row['cityId'] = data['data']['city_id']
        # 景区类型
        if len(data['data']['categorys']) >= 1:
            if 'category_name' in data['data']['categorys'][0].keys():
                row['categoryName'] = data['data']['categorys'][0]['category_name']
        # 景区等级
        if 'grade' in data['data'].keys():
            row['grade'] = data['data']['grade']
        # 门票类型,随时退
        if 'tag_list' in data['data'].keys():
            tag_list_data = data['data']['tag_list']
            tag_list = []
            for tag in tag_list_data:
				tag_list.append(tag['title'])
            row['tagList'] = tag_list
        if data['data']['city_id'] in cityDict.keys():
            row['city'] = cityDict[data['data']['city_id']]
        row['districtId'] = data['data']['district_id']
        if data['data']['district_id'] in cityDict.keys():
            row['district'] = cityDict[data['data']['district_id']]
        row['spotId'] = data['data']['id']
        product_info = data['data']['product_info']
        if len(data['data']['poi']) == 2:
            row['geo'] = {'lon': data['data']['poi'][0],
                          'lat': data['data']['poi'][1]}
        for ticket_class in product_info:
            for ticket in ticket_class['list']:
                m_row = copy.deepcopy(row)
                m_row['resource_id'] = ticket['resource_id']  # 用这个id去ti_selfticket表查对应的ticket_id
                m_row['price'] = ticket['price']
                m_row['name'] = ticket['name']
                rows.append(m_row)
    return rows



def getTicket(spotList):
    rows = []
    for spot in spotList:
        context = _getTicket(spot['spot_id'])
        data = json.loads(context)
        row = {}
        if data['msg'] != 'OK':
            return
        row['category'] = 'ticket'
        row['categoryDisplay'] = '门票'
        row['thumbUrl'] = data['data']['img_url']
        row['cityId'] = data['data']['city_id']
        # 景区类型
        if len(data['data']['categorys']) >= 1:
            if 'category_name' in data['data']['categorys'][0].keys():
                row['categoryName'] = data['data']['categorys'][0]['category_name']
        # 景区等级
        if 'grade' in data['data'].keys():
            row['grade'] = data['data']['grade']
        # 门票类型,随时退
        if 'tag_list' in data['data'].keys():
            tag_list_data = data['data']['tag_list']
            tag_list = []
            for tag in tag_list_data:
				tag_list.append(tag['title'])
            row['tagList'] = tag_list
        if data['data']['city_id'] in cityDict.keys():
            row['city'] = cityDict[data['data']['city_id']]
        row['districtId'] = data['data']['district_id']
        if data['data']['district_id'] in cityDict.keys():
            row['district'] = cityDict[data['data']['district_id']]
        row['spotId'] = data['data']['id']
        product_info = data['data']['product_info']
        if len(data['data']['poi']) == 2:
            row['geo'] = {'lon': data['data']['poi'][0],
                          'lat': data['data']['poi'][1]}
        for ticket_class in product_info:
            for ticket in ticket_class['list']:
                m_row = copy.deepcopy(row)
                m_row['resource_id'] = ticket['resource_id']  # 用这个id去ti_selfticket表查对应的ticket_id
                m_row['price'] = ticket['price']
                m_row['name'] = ticket['name']
                rows.append(m_row)
    return rows


def getRowFromDict(m_dicts, key, val):
    result = {}
    for m_dict in m_dicts:
        if m_dict[key] == val:
            result = m_dict
            return result
    return result


def getRowFromDicts(m_dicts, key, val):
    result = []
    for m_dict in m_dicts:
        if m_dict[key] == val:
            result.append(m_dict)
    return result


def hasValfromDicts(m_dicts, key, val):
	for m_dict in m_dicts:
		if m_dict[key] == val:
			return True
	return False		


def hasKey(m_dict,key):
	if key in m_dict.keys():
		return True
	return False


def ticket_index(producer, index, ticketList):
    for ticket in ticketList:  #
        try: 
            body = {
                "Action": "index",
                "DocumentID": ticket['spotId'],
                "Index": index,
                "body": {
                "ticketPrice": int(ticket['price']),
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
                "isBookingLimit": ticket['isBookingLimit'],
                "isPosFace": ticket['isPosFace'],
                "isPreferential": ticket['isPreferential'],
                "isqrCode": ticket['isqrCode'],
                "tags": ticket['tagList'],
                "scenicType": ticket['categoryName'] if(hasKey(ticket,'categoryName')) else None,
                "ticketResourceId": ticket['resource_id'] if(hasKey(ticket,'resource_id')) else None,
                "refundNewType": ticket['refundNewType'],
                "city": ticket['city'],
                "cityId": ticket['cityId'],
                "ticketName": ticket['name'].strip(),
                #"title": ticket['name'].strip(),
                "title": ticket['scenicName'].strip(),
                "districtId": ticket['districtId'],
                "grade": ticket['grade'],
                "category": ticket['category'],
                "categoryDisplay": ticket['categoryDisplay'],
                "thumbUrl": ticket['thumbUrl'],
                "isVirtualScenic": ticket['isVirtualScenic'],
                "scenicId": ticket['spotId'],
                "ticketId": ticket['ticketId'],
                "url":"yyn://ticket_detail?id="+ticket['spotId'],
                "_scenicNameTxt": ticket['scenicName'].strip()
                }
            }
        except KeyError:
            continue
			
        if 'district' in ticket.keys():
            # print ticket['district']
            body["body"]["district"] = ticket['district']
        # print body['body']
        if 'geo' in ticket.keys():
            body["body"]["geo"] = ticket["geo"]
        if 'ticketTypeName' in ticket.keys():
            body["body"]["ticketTypeName"] = ticket["ticketTypeName"]

        body["body"]["isGroupPurchase"] = False
        if 'groupPurchase' in ticket.keys():
            if ticket["groupPurchase"] in ("精选促销","团购精选"):
                body["body"]["isGroupPurchase"] = True

        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    # producer.close()
    print 'complete ticket index num : {0}'.format(len(ticketList))


def ticket_update(producer, index, ticketList):
    for ticket in ticketList:  #
        body = {
            "Action": "update",
            "DocumentID": ticket['ticketId'],
            "Index": index,
            "body": {
                "ticketPrice": int(ticket['price']),
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    # producer.close()
    print 'complete ticket update num : {0}'.format(len(ticketList))


def ticket_del(producer, index, ticketList, selfticketList):
    delList = []
    for selfticket in selfticketList:
        if getRowFromDict(ticketList, 'ticketId', selfticket['ticket_id']) == {}:  # 如果不在黑才接口返回的门票列表里,则删除
            delList.append(selfticket['ticket_id'])

    for del_id in delList:  #
        body = {
            "Action": "delete",
            "DocumentID": del_id,
            "Index": index,
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    #producer.close()
    print 'complete ticket del num : {0}'.format(len(delList))


def ticket_combin(ticketList, selfticketList, scenicList, virScenicList):
    rows = []
    for row in ticketList:
        selfticket = getRowFromDict(selfticketList, '_id', row['resource_id'])
        if selfticket == {}:
            continue
        #scenic = getRowFromDict(scenicList, '_id', row['spotId'])#用黑才接口的spotid来匹配景区
        scenic = getRowFromDict(scenicList, '_id', selfticket['card_id'])#用selfticket.mongo的cardid来匹配景区
        row['spotId']=selfticket['card_id']
        row['ticketId'] = (selfticket['ticket_id'] if 'ticket_id' in ((selfticket.keys())) else '')
        row['isBookingLimit'] = (selfticket['is_booking_limit'] if 'is_booking_limit' in ((selfticket.keys())) else '')
        row['isPosFace'] = (selfticket['is_pos_face'] if 'is_pos_face' in ((selfticket.keys())) else '')
        row['isPreferential'] = (selfticket['is_preferential'] if 'is_preferential' in ((selfticket.keys())) else '')
        if 'is_qr_code' in selfticket.keys():
            row['isqrCode'] = (False if (0 == int(selfticket['is_qr_code'])) else True)
        if 'refund_new_type' in selfticket.keys():
            row['refundNewType'] = int(selfticket['refund_new_type'])
        row['ticketTypeName']=(selfticket['ticketTypeName'] if 'ticketTypeName' in ((selfticket.keys())) else '')
        row['groupPurchase']=(selfticket['groupPurchase'] if 'groupPurchase' in ((selfticket.keys())) else '')
        if scenic:
            row['grade'] = scenic['grade']
            row['scenicName'] = scenic['name']
        if row['spotId'] in virScenicList:
            row['isVirtualScenic'] = True
        else:
            row['isVirtualScenic'] = False

        rows.append(row)
    ticketList = rows


def scenic_update(producer, scenicList,spotList,ticketList):
	up_list=[]
	for spot in spotList:
		row = {'id':spot['spot_id'],'lowest_price':spot['price_from']}
		up_list.append(row)
	for up in up_list:  # 更新景区最低价格
		body = {
			"Action": "update",
			"DocumentID": up['id'],
			"Index": 'alias_cn_yyn_scenic',
			"body": {
				"lowestPrice": int(up['lowest_price']),
				"_indexTime": arrow.now('Asia/Chongqing').timestamp,
			}
		}
		producer.send('curious-index', json.dumps(body).encode('utf-8'))
	producer.flush()
	print 'complete scenic_lowest_price update num : {0}'.format(len(up_list))
	del_list = []
	for scenic in scenicList:
		if getRowFromDict(spotList, 'spot_id', scenic['_id']) == {}:  # 如果不在黑才接口返回的门票列表里,则删除
			row = {'id':scenic['_id']}
			del_list.append(row)
	for de in del_list:  # 更新景区最低价格
		body = {
			"Action": "update",
			"DocumentID": de['id'],
			"Index": 'alias_cn_yyn_scenic',
			"body": {
				"lowestPrice": None,
				"_indexTime": arrow.now('Asia/Chongqing').timestamp,
			}
		}
		producer.send('curious-index', json.dumps(body).encode('utf-8'))
	producer.flush()
	print 'complete scenic_lowest_price clear num : {0}'.format(len(del_list))
	producer.close() # 关闭客户端
		 
	pass


def ticketPro(kafkaHost, index):
    scenicList = []  # ti_scenic
    selfticketList = []  # ti_selfticket
    virScenicList = []
    spotList = getSpot()  # 黑才的接口
    ticketList = getTicket(spotList)  # 黑才的接口2
    #ticketList = getTicket2()  # 黑才的接口2
    
    #conn = pymongo.MongoClient('172.18.5.7', username='bdc_api_test', password='0YhMDvK0N^MWqz12')
    conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    selfticket_db = conn['ti_selfticket']
    selfticket_col = selfticket_db['cn']
    scenic_db = conn['ti_scenic']
    scenic_col = scenic_db['cn']
    # scenic
    scenic_data = scenic_col.find({'self_ticket.ticket_id': {'$exists': True}},
                                  {'self_ticket.ticket_id': 1, '_id': 1, 'city_id': 1, 'district_id': 1, 'grade': 1, 'name':1,
                                   'image.thumb_url': 1, 'is_appoint':1})
    for i in scenic_data:
        #if 'is_appoint' in i.keys():
        #    i[] = i['is_appoint']
        if 'image' in i.keys():
            i['image'] = i['image']['thumb_url']
        if 'grade' in i.keys():
            i['grade'] = int(i['grade'])
        self_tickets = i['self_ticket']['ticket_id']
        del i['self_ticket']
        for ticketId in self_tickets:
            i['ticketId'] = ticketId
            scenicList.append(i)
    selfticket_data = selfticket_col.find({'ticket_id': {'$exists': True},'card_id': {'$exists': True}},
                                          {'_id': 1,'card_id':1, 'name': 1, 'ticket_id': 1, 'is_booking_limit': 1,
                                           'is_preferential': 1, 'is_pos_face': 1, 'refund_new_type': 1,
                                           'is_qr_code': 1,'ticketTypeName':1,'groupPurchase':1})
    for i in selfticket_data:
        selfticketList.append(i)
    virScenic_data = scenic_col.find({'status': 0}, {'_id': 1})
    for virScenic in virScenic_data:
        virScenicList.append(virScenic['_id'])
    # print ('scenic num : {0}\n'.format(len(scenicList)))
    # print ('selfticket num : {0}\n'.format(len(selfticketList)))
    # print ('ticket num : {0}\n'.format(len(ticketList)))
    # 组合门票数据
    ticket_combin(ticketList, selfticketList, scenicList, virScenicList)

    ticket_list = []
    for i in ticketList:
        if 'ticketTypeName' in i.keys() and  i['ticketTypeName']  in ('景区门票','直通车+景区门票','景区体验','多景点联票','特惠套餐','演出/展览','休闲娱乐','特色体验','景区套票','酒店套餐','酒店玩乐','酒店美食'):
            ticket_list.append(i)
    ticket_list = ticket2scenic(ticket_list)

    producer = KafkaProducer(bootstrap_servers=kafkaHost)
    ticket_index(producer, index, ticket_list)
    #ticket_update(producer, index, ticket_list)
    #ticket_del(producer, index, ticket_list, selfticketList)
    scenic_update(producer, scenicList,spotList,ticket_list)


def initPgDB(dbName, user, passwd, host, port):
    conn = psycopg2.connect(database=dbName,
                            user=user,
                            password=passwd,
                            host=host,
                            port=port)
    # cur = conn.cursor()
    return conn


# 更新景区索引实时人数
def update_index_scenic_pop(producer):
    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    conn = initPgDB('economicanalysis', 'economic', 'economic@ynty1', '172.18.100.43', '5432')
    scenic_db = mongo_conn['ti_scenic']
    scenic_col = scenic_db['cn']
    scenic_data = scenic_col.find({'type':1}, {'_id': 1})
    scenicList = []
    for i in scenic_data:
        scenicList.append(i['_id'])
    up_list = _update_index_scenic_pop(conn)
    del_list = []
    for id in scenicList: 
        if( hasValfromDicts(up_list, 'id', id) == False):
            del_list.append(id)
    for up in up_list:  # 更新景区实时人数
        body = {
            "Action": "update",
            "DocumentID": up['id'],
            "Index": 'alias_cn_yyn_scenic',
            "body": {
                "persons": int(up['persons']),
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }

        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete scenic_persons update num : {0}'.format(len(up_list))
    for de in del_list:  # 删除景区实时人数
        body = {
            "Action": "update",
            "DocumentID": de,
            "Index": 'alias_cn_yyn_scenic',
            "body": {
                "persons": None,
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete scenic_persons clear num : {0}'.format(len(del_list))
    producer.close() # 关闭客户端


def _update_index_scenic_pop(conn):
    sql = """with id as (select id,mongo_scenic_id from gl_region where mongo_scenic_id notnull),
        data as (select mongo_scenic_id,x.id,persons from
        (select id, value as persons, time
             from (select id, value, time, dense_rank() over (partition by id order by time desc) as pos
            from gl_population where id in (select id from id) and  time>= now() + interval '-3 hours') pp
             where pos = 1)x left join id on id.id=x.id)
        select mongo_scenic_id,id,persons from data;"""
    cur = conn.cursor()
    cur.execute(sql)
    rst = cur.fetchall()
    rows = []
    for row in rst:
        rows.append({ 'id': row[0], 'persons': row[2]})
    return rows


def getCat(cat_id):
	catFirst=None
	catSec=None
	if cat_id in catDict.keys():
		if 'par' in catDict[cat_id].keys():
			catFirst =catDict[cat_id]['par']
			catSec = catDict[cat_id]['name']
		else :
			catFirst = catDict[cat_id]['name']
	return catFirst,catSec 


# 更新诚选商品
def update_index_shopping(producer):
    #mongo_conn = pymongo.MongoClient('172.18.5.7', username='bdc_api_test', password='0YhMDvK0N^MWqz12')
    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    shopping_db = mongo_conn['ti_shopping']
    shopping_col = shopping_db['cn']
    shopping_data = shopping_col.find({'gid':{'$exists':True},'comm_id':{'$exists':True},'marketable':True},
    #shopping_data = shopping_col.find({'gid':{'$exists':True},'comm_id':{'$exists':True},'marketable':True,'status':1},
		 {'_id': 1, 'comm_id':1, 'status':1, 'gid':1, 'comm_name':1, 'labels':1, 'original_price':1, 'comm_detail_url': 1, 'comm_img_url':1, 'brief':1,'labels':1,
         'cat_id':1, 'credit_score':1})
    del_data = shopping_col.find({'marketable':False}, {'_id': 1, 'comm_id':1, 'status':1, 'gid':1, 'comm_name':1, 'labels':1, 'original_price':1, 'comm_img_url':1})
    shopping_list = []
    del_list = []
    for item in del_data: 
        del_list.append(item['comm_id'])
    for shop in shopping_data:  # 建立索引
        shopping_list.append(shop['comm_id'])
        data =  {
				"category": 'shopping',
				"categoryDisplay": '特产',
				"title": shop['comm_name'],
				"_indexTime": arrow.now('Asia/Chongqing').timestamp,
				"labelsTxt":(shop['labels'] if(hasKey(shop,'labels')) else None),
				"detailUrl":shop['comm_detail_url'],
				"price":int(round ( (shop['original_price'])*100)),
				"thumbUrl":(shop['comm_img_url'] if(hasKey(shop,'comm_img_url')) else None),
				"_briefTxt":(shop['brief'] if(hasKey(shop,'brief')) else None),
				"_fullTxt":(shop['intro'] if(hasKey(shop,'intro')) else None),
				"creditScore":(shop['credit_score'] if(hasKey(shop,'credit_score')) else None),
            }
        if 'cat_id' in shop.keys():
            catFirst,catSec = getCat(str(int(shop['cat_id'])))
            if catFirst: 
                data['firstKind'] = catFirst
            if catSec:
                data['secondKind'] = catSec
        data['isGroupPurchase'] = False
        if 'labels' in shop.keys():
            for item in shop['labels']:
                if item in ('精选促销','团购精选'):
                    data['isGroupPurchase'] = True
        for k,v in data.items():
            if v==None:
                del data[k]

        body = {
			"Action": "index",
			"DocumentID": shop['comm_id'],
			"Index": 'alias_cn_yyn_shopping',
        }
        body['body'] = data
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
	producer.flush()
    print 'complete shopping index num : {0}'.format(len(shopping_list))
    for de in del_list:  # 删除下架的商品
        body = {
			"Action": "delete",
			"DocumentID": de,
			"Index": 'alias_cn_yyn_shopping',
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete shopping del num : {0}'.format(len(del_list))
    producer.close() # 关闭客户端


def get_video_category():
    category_url = 'https://gw.ybsjyyn.com/app/page/home/every_day_yn'
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Accept-Encodong': 'gzip, deflate, br'
    }
    res = requests.get(category_url, headers=headers)
    res.encoding = 'utf-8'
    menu_list = json.loads(res.text)['data']['menu_list']
    result = []
    for menu in menu_list:
        menu_id = menu['id']
        menu_title = menu['title']
        if menu_title == u'推荐':
            menu_title = u'编辑推荐'
        category_url = 'https://gw.ybsjyyn.com/app/page/home/every_day_yn?tab_id={id}'.format(id=menu_id)
        res = requests.get(category_url, headers=headers)
        content_list = json.loads(res.text)['data']['content_list']
        for context in content_list:
            result.append({'id': context['id'], 'theme': menu_title})
    return result



def update_index_video(producer):
    video_theme = get_video_category()
    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    scenic_db = mongo_conn['ti_scenic']
    scenic_col = scenic_db['cn']
    scenic_data = scenic_col.find({'grade':{'$exists': True}}, {'_id': 1, 'grade':1})
    video_db = mongo_conn['ti_video']
    video_col = video_db['cn']
    video_data = video_col.find({'status':1,'scenic_id':{'$exists': True}},{'_id':1,'scenic_id':1})
    video_cn_list = []
    video_item_list = []
    del_list = []
    for i in video_data:
        video_cn_list.append(i['_id'])
        video_item_list.append(i)
    for id in video_cn_list:
        if (hasValfromDicts(video_theme, 'id', id) == False):
            del_list.append(id)

    for up in video_theme:  # update直播类型
        body = {
            "Action": "update",
            "DocumentID": up['id'],
            "Index": 'alias_cn_yyn_video',
            "body": {
                "themeType": up['theme'],
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete video_themeType update num : {0}'.format(len(video_theme))

    for de in del_list:      # 删除直播类型
        body = {
            "Action": "update",
            "DocumentID": de,
            "Index": 'alias_cn_yyn_video',
            "body": {
                "themeType": None,
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete video_themeType clear num : {0}'.format(len(del_list))

    scenic_ds = []
    data = []
    for sc in scenic_data:
        scenic_ds.append(sc)

    for vi in video_item_list:
        for sc in scenic_ds:
            if vi['scenic_id'] == sc['_id'] and sc['grade'] != '':
                data.append({'_id':vi['_id'],'grade':sc['grade']})
        
    for up in data:  # 更新video景区等级
		body = {
			"Action": "update",
			"DocumentID": up['_id'],
			"Index": 'alias_cn_yyn_video',
			"body": {
				"grade": int(up['grade']),
				"_indexTime": arrow.now('Asia/Chongqing').timestamp,
			}
		}
		producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    pass


def get_live_category():
    url = 'https://mp-gw.ybsjyyn.com/sceniclive/live/scenic_live'
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        'Accept-Encodong': 'gzip, deflate, br'
    }
    res = requests.get(url, headers=headers)
    res.encoding = 'utf-8'
    data = json.loads(res.text)
    return data['data']['category']


def match_scenic_live_id(scenic_live_category_data):
    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    scenic_live_cn_db = mongo_conn['ti_sceniclive']
    scenic_live_cn_col = scenic_live_cn_db['cn']
    scenic_live_cn_data = scenic_live_cn_col.find({'sub_live_ids':{'$exists': True}}, {'_id': 1,'sub_live_ids':1})
    ids = []
    result_ids = []
    for item in scenic_live_cn_data:
        if len(item['sub_live_ids'])>0:
            scenic_live = item['_id']
            for sub_live_ids in item['sub_live_ids']:
                for i in json.loads(sub_live_ids):
                    ids.append({'sceniclive':scenic_live,'id':i['id']})
    for live in ids:
        for scenic in scenic_live_category_data:
            if live['sceniclive'] == scenic['scenic_live_id']:
                result_ids.append({'id':live['id'],'category_name':scenic['category_name']})
    return result_ids
    



def set_live_category(category_l):
    scenic_live_category_data = []
    live_category_data = []
    for item in category_l:
        category_name = item['group_name']
        group_id = item['group_id']
        scenic_live_category_data += _get_live_category(group_id, category_name)
    live_category_data = match_scenic_live_id(scenic_live_category_data)
    
    for up in live_category_data:  # update直播类型
        body = {
            "Action": "update",
            "DocumentID": up['id'],
            "Index": 'alias_cn_yyn_live',
            "body": {
                "scenicType": up['category_name'],
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete live_scenicType update num : {0}'.format(len(live_category_data))

    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    live_cn_db = mongo_conn['ti_live']
    live_cn_col = live_cn_db['cn']
    live_cn_data = live_cn_col.find({'status': 1}, {'_id': 1})
    live_cn_List = []
    for i in live_cn_data:
        live_cn_List.append(i['_id'])
    del_list = []
    for id in live_cn_List:
        if (hasValfromDicts(live_category_data, 'id', id) == False):
            del_list.append(id)

    for de in del_list:      # 删除直播类型
        body = {
            "Action": "update",
            "DocumentID": de,
            "Index": 'alias_cn_yyn_live',
            "body": {
                "scenicType": None,
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete live_scenicType clear num : {0}'.format(len(del_list))
    pass


def _get_live_category(group_id, category_name):
    # 这个接口取到的是scenic_live 的id
    url = 'https://mp-gw.ybsjyyn.com/sceniclive/live/scenic_page'
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        # 'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
        # 'Accept-Encodong': 'gzip, deflate, br',
        "charset": "UTF-8"
    }
    url_param = 'theme_ids=[]&city_id=[]&star=[]&cate_ids=["{group_id}"]&context='.format(
        group_id=group_id).encode('utf-8')
    redata = my_urlencode(url_param)
    res = requests.post(url+'?'+redata, headers=headers)
    res.encoding = 'utf-8'
    data = json.loads(res.text)
    live_list = data['data']['select_list']['list']
    live_data = []
    for live in live_list:
        live_data.append({
            'category_name': category_name,
            'scenic_live_id': live['id']
        })
    return live_data


# 自己的url编码
def my_urlencode(url):
    split_l = url.split('&')
    data = []
    for sp in split_l:
        split_item_l = sp.split('=')
        data.append(split_item_l[0]+'='+urllib.quote(split_item_l[1]))
    return '&'.join(data)


def update_index_live(producer):
    # 直播主题
    live_category = get_live_category()
    set_live_category(live_category)
    pass


# 更新诚选商品数据到知云推荐的ES
def update_index_cloud9_shopping(producer):
    mongo_conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    shopping_db = mongo_conn['ti_shopping']
    shopping_col = shopping_db['cn']
    shopping_data = shopping_col.find({'gid':{'$exists':True},'comm_id':{'$exists':True},'marketable':True},
		  {'_id': 1, 'comm_id':1, 'status':1, 'gid':1, 'comm_name':1, 'labels':1,'cur_price':1,'busi_addr':1,
         'ctime':1, 'original_price':1, 'comm_detail_url': 1, 'comm_img_url':1, 'brief':1,
         'cat_id':1, 'credit_score':1})
    del_data = shopping_col.find({'marketable':False}, {'_id': 1, 'comm_id':1, 'status':1, 'gid':1, 'comm_name':1, 'labels':1, 'original_price':1, 'comm_img_url':1})
    shopping_list = []
    del_list = []
    for item in del_data: 
        del_list.append(item['comm_id'])
    for shop in shopping_data:  # 建立索引
        shopping_list.append(shop['comm_id'])
        data =  {
				"type": 'shopping',
				"typeText": '特产',
				"title": shop['comm_name'],
				"_indexTime": arrow.now('Asia/Chongqing').timestamp,
				"_pubTime": (shop['ctime'] if(hasKey(shop,'ctime')) else None),
                "_status": 1,
				"url":shop['comm_detail_url'],
				"thumbUrl":(shop['comm_img_url'] if(hasKey(shop,'comm_img_url')) else None),
				"credit":(shop['credit_score'] if(hasKey(shop,'credit_score')) else None),
				"originalPrice":int(round ( (shop['original_price'])*100)),
				"price":int(round ( (shop['cur_price'])*100)),
                "address":(shop['busi_addr'] if(hasKey(shop,'busi_addr')) else None)
			}
        if data['price'] == 0 or data['originalPrice'] == 0 :
            data['_status'] = 0
            data['_statusReason'] = '价格为0'

        if data['url'] != '':
            if '?' in data['url']:
                data['url'] += '&titleBar=hidden&screen=full'
            else:
                data['url'] += '?titleBar=hidden&screen=full'


        tags = []
        if 'cat_id' in shop.keys():
            catFirst,catSec = getCat(str(int(shop['cat_id'])))
            if catFirst: 
                tags.append(catFirst)
            if catSec:
                tags.append(catSec)
        if 'labels' in shop.keys():	
            for tag in shop['labels']:
                tags.append(tag)
        data['tags'] = tags
        if 'busi_addr' in shop.keys():
            for cityList in cityLists:
                if cityList[0] == shop['busi_addr']:
                    data['cityId'] = cityList[3]
                    data['city'] = cityList[1]


        for k,v in data.items():
            if v==None:
                del data[k]
            body = {
            "Action": "index",
            "DocumentID": shop['comm_id'],
            "Index": 'alias_cloud9_cn_yyn_shopping',
            }
        body['body'] = data
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete shopping index num : {0}'.format(len(shopping_list))
    for de in del_list:  # 删除下架的商品
        body = {
			"Action": "delete",
			"DocumentID": de,
			"Index": 'alias_cloud9_cn_yyn_shopping',
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    print 'complete shopping del num : {0}'.format(len(del_list))
    producer.close() # 关闭客户端


en_city_dict = {
    "380":"Chuxiong","384":"Honghe","379":"Baoshan","386":"Lijiang","389":"Pu'er","387":"Lincang","390":"Qujing","381":"Dali","385":"Kunming","392":"Xishuangbanna","383":"Diqing","393":"Yuxi","382":"Dehong","388":"Nujiang","391":"Wenshan","394":"Zhaotong","3007":"Changning","3008":"Longling","3009":"Longyang","3010":"Shidian","3011":"Tengchong","3012":"Chuxiong","3013":"Dayao","3014":"Lufeng","3015":"Mouding","3016":"Nanhua","3017":"Shuangbai","3018":"Wuding","3019":"Yao'an","3020":"Yongren","3021":"Yuanmou","3022":"Binchuan","3023":"Dali","3024":"Eryuan","3025":"Heqing","3026":"Jianchuan","3027":"Midu","3028":"Nanjian","3029":"Weishan","3030":"Xiangyun","3031":"Yangbi","3032":"Yongping","3033":"Yunlong","3034":"Ruili","3036":"Lianghe","3037":"Yingjiang","3038":"Longchuan","3039":"Deqin","3040":"Weixi","3041":"Xianggelila","3042":"Gejiu","3043":"Hekou","3044":"Honghe","3045":"Jianshui","3046":"Jinping","3047":"Kaiyuan","3048":"Luxi","3049":"Luchun","3050":"Mengzi","3051":"Mile","3052":"Pingbian","3053":"Shiping","3054":"Yuanyang","3055":"Anning","3056":"Chenggong","3057":"Dongchuan","3058":"Fumin","3059":"Guandu","3060":"Jinning","3061":"Luquan","3062":"Panlong","3063":"Shilin","3064":"Songming","3065":"Wuhua","3066":"Xishan","3067":"Xundian","3068":"Yiliang","3070":"Huaping","3071":"Ninglang","3072":"Yongsheng","3073":"Yulong","3074":"Cangyuan","3075":"Fengqing","3076":"Gengma","3077":"Linxiang","3078":"Shuangjiang","3079":"Yongde","3080":"Yunxian","3081":"Zhenkang","3082":"Lushui","3083":"Fugong","3084":"Gongshan","3085":"Lanping","3086":"Jiangcheng","3087":"Jingdong","3088":"Jinggu","3089":"Lancang","3090":"Menglian","3091":"Mojiang","3092":"Ning'er","3093":"Simao","3094":"Ximeng","3095":"Zhenyuan","3096":"Fuyuan","3097":"Huize","3098":"Luliang","3099":"Luoping","3100":"Malong","3101":"Qilin","3102":"Shizong","3103":"Xuanwei","3104":"Zhanyi","3105":"Funing","3106":"Guangnan","3107":"Malipo","3108":"Maguan","3113":"Jinghong","3114":"Menghai","3115":"Mengla","3116":"Chengjiang","3117":"Eshan","3118":"Hongta","3119":"Huaning","3120":"Jiangchuan","3121":"Tonghai","3122":"Xinping","3123":"Yimen","3124":"Yuanjiang","3125":"Zhaoyang","3126":"Ludian","3127":"Qiaojia","3128":"Yanjin","3129":"Daguan","3130":"Yongshan","3131":"Suijiang","3132":"Zhenxiong","3133":"Yiliang","3134":"Weixin","3135":"Shuifu","3340":"Mangshi","3345":"Qiubei","3346":"Xichou","3347":"Yanshan","3110":"Wenshan","3069":"Gucheng, Lijiang","80005":"Gaoligongshan Resort","80001":"Economic Development Zone, Kunming","80002":"Yangzonghai Resort","80003":"Dianchi Resort","80004":"High-tech Zone, Kunming","80001":"Economic Development Zone, Qujing","80001":"JingkaiEconomic Development Zone, Dali","80008":"Gucheng Resort, Dali","80009":"Haidong Development Zone, Dali","80010":"Industry Zone, Lincang","80011":"Bianhe"
}


def getEnCityName(id):
    if id in en_city_dict.keys():
        return en_city_dict[id]
    return None


def update_index_en_Activities(producer):
    url = 'http://mp.ybsjyyn.com/api/c-article/activity-list?type=3&display_limit=3&p=1&page_size=1000'
    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }
    res = requests.get(url, headers=headers)
    res.encoding = 'utf-8'
    data = json.loads(res.text)['data']['list']
    indexList = []
    for item in data:
        if item['status'] == 0:
            continue
        row = {}
        row['category'] = 'activities'
        row['categoryDisplay'] = 'Activities'
        row['start_time'] = item['start_time']
        row['end_time'] = item['end_time']
        row['title'] = item['title']
        row['sloganTxt'] = item['description']
        row['city_id'] = item['city_id']
        row['city'] = getEnCityName(item['city_id'])
        row['district_id'] = item['district_id']
        row['district'] = getEnCityName(item['district_id'])
        row['address'] = item['address']
        row['author'] = item['author_name']
        row['_id'] = item['_id']
        row['thumbUrl'] = item['cover_image']

        indexList.append(row)
    for Activitie in indexList:  # 建立索引
        body = {
            "Action": "index",
            "DocumentID": Activitie['_id'],
            "Index": 'alias_en_yyn_activities',
            "body": {
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
                'category': Activitie['category'],
                'categoryDisplay': Activitie['categoryDisplay'],
                'startTime': Activitie['start_time'],
                'endTime': Activitie['end_time'],
                'title': Activitie['title'],
                'sloganTxt': Activitie['sloganTxt'],
                'city_id': Activitie['city_id'],
                'city': Activitie['city'],
                'district_id': Activitie['district_id'],
                'district': Activitie['district'],
                'address': Activitie['address'],
                'author': Activitie['author'],
                'thumbUrl': Activitie['thumbUrl'],
            }
        }
        producer.send('curious-index', json.dumps(body).encode('utf-8'))
        print body
    producer.flush()
    print 'complete alias_en_yyn_activities index num : {0}'.format(len(indexList))


def traffic_index(producer, index, ticketList):
    for ticket in ticketList:  #
        ticket['category'] = 'traffic'
        ticket['categoryDisplay'] = '交通出行'
        try: 
            body = {
                "Action": "index",
                "DocumentID": ticket['spotId'],
                "Index": index,
                "body": {
                "ticketPrice": int(ticket['price']),
                "_indexTime": arrow.now('Asia/Chongqing').timestamp,
                "isBookingLimit": ticket['isBookingLimit'],
                "isPosFace": ticket['isPosFace'],
                "isPreferential": ticket['isPreferential'],
                "isqrCode": ticket['isqrCode'],
                "tags": ticket['tagList'],
                "scenicType": ticket['categoryName'] if(hasKey(ticket,'categoryName')) else None,
                "ticketResourceId": ticket['resource_id'] if(hasKey(ticket,'resource_id')) else None,
                "refundNewType": ticket['refundNewType'],
                "city": ticket['city'],
                "cityId": ticket['cityId'],
                "ticketName": ticket['name'].strip(),
                #"title": ticket['name'].strip(),
                "title": ticket['scenicName'].strip(),
                "districtId": ticket['districtId'],
                "grade": ticket['grade'],
                "category": ticket['category'],
                "categoryDisplay": ticket['categoryDisplay'],
                "thumbUrl": ticket['thumbUrl'],
                "isVirtualScenic": ticket['isVirtualScenic'],
                "scenicId": ticket['spotId'],
                "ticketId": ticket['ticketId'],
                "url":"yyn://ticket_detail?id="+ticket['spotId'],
                "_scenicNameTxt": ticket['scenicName'].strip()
                }
            }
        except KeyError:
            continue
			
        if 'district' in ticket.keys():
            # print ticket['district']
            body["body"]["district"] = ticket['district']
        # print body['body']
        if 'geo' in ticket.keys():
            body["body"]["geo"] = ticket["geo"]
        if 'ticketTypeName' in ticket.keys():
            body["body"]["ticketTypeName"] = ticket["ticketTypeName"]

        body["body"]["isGroupPurchase"] = False
        if 'groupPurchase' in ticket.keys():
            if ticket["groupPurchase"] in ("精选促销","团购精选"):
                body["body"]["isGroupPurchase"] = True
            

        producer.send('curious-index', json.dumps(body).encode('utf-8'))
    producer.flush()
    # producer.close()
    print 'complete traffic index num : {0}'.format(len(ticketList))


def ticket2scenic(traffic_list): #现在所有门票要展示最低价的景区
    temp = {}
    result = []
    for i in traffic_list:
        if i['spotId'] in temp.keys():
            temp[i['spotId']].append(i)
        else:
            temp[i['spotId']] = [i]
    for scenicid in temp.keys():
        _min_traffic = {}
        _min = 1000000
        for i in temp[scenicid]:
            if i['price'] < _min:
                _min = i['price']
                _min_traffic = i
        result.append(_min_traffic)
    return result


def update_index_traffic(producer):
    index = 'idx_cn_yyn_traffic_20211230'
    #index = 'alias_cn_yyn_traffic'
    scenicList = []  # ti_scenic
    selfticketList = []  # ti_selfticket
    virScenicList = []
    spotList = getSpot()  # 黑才的接口
    ticketList = getTicket(spotList)  # 黑才的接口2
    #ticketList = getTicket2()  # 黑才的接口2

    #conn = pymongo.MongoClient('172.18.5.7', username='bdc_api_test', password='0YhMDvK0N^MWqz12')
    conn = pymongo.MongoClient('172.18.102.3', username='bdcrw', password='dSy9j49CcCQIXkQqxioK')
    selfticket_db = conn['ti_selfticket']
    selfticket_col = selfticket_db['cn']
    scenic_db = conn['ti_scenic']
    scenic_col = scenic_db['cn']
    # scenic
    scenic_data = scenic_col.find({'self_ticket.ticket_id': {'$exists': True}},
                                  {'self_ticket.ticket_id': 1, '_id': 1, 'city_id': 1, 'district_id': 1, 'grade': 1, 'name':1,
                                   'image.thumb_url': 1, 'is_appoint':1})
    for i in scenic_data:
        #if 'is_appoint' in i.keys():
        #    i[] = i['is_appoint']
        if 'image' in i.keys():
            i['image'] = i['image']['thumb_url']
        if 'grade' in i.keys():
            i['grade'] = int(i['grade'])
        self_tickets = i['self_ticket']['ticket_id']
        del i['self_ticket']
        for ticketId in self_tickets:
            i['ticketId'] = ticketId
            scenicList.append(i)
    selfticket_data = selfticket_col.find({'ticket_id': {'$exists': True},'card_id': {'$exists': True}},
                                          {'_id': 1,'card_id':1, 'name': 1, 'ticket_id': 1, 'is_booking_limit': 1,
                                           'is_preferential': 1, 'is_pos_face': 1, 'refund_new_type': 1,
                                           'is_qr_code': 1,'ticketTypeName':1,'groupPurchase':1})
    for i in selfticket_data:
        selfticketList.append(i)
    virScenic_data = scenic_col.find({'status': 0}, {'_id': 1})
    for virScenic in virScenic_data:
        virScenicList.append(virScenic['_id'])
    # print ('scenic num : {0}\n'.format(len(scenicList)))
    # print ('selfticket num : {0}\n'.format(len(selfticketList)))
    # print ('ticket num : {0}\n'.format(len(ticketList)))
    # 组合门票数据
    ticket_combin(ticketList, selfticketList, scenicList, virScenicList)

    traffic_list = []
    for i in ticketList:
        if 'ticketTypeName' in i.keys() and  i['ticketTypeName']  in ('直通车','接驳车','接送机','高铁站服务','租车','包车','机场服务'):
            traffic_list.append(i)
    print len(traffic_list)
    traffic_list = ticket2scenic(traffic_list)
    print '-----------------------------------------'
    for i in traffic_list:
        print i
    producer = KafkaProducer(bootstrap_servers=kafkaHost)
    traffic_index(producer, index, traffic_list)
#   ticket_update(producer, index, ticketList)
#   ticket_del(producer, index, ticketList, selfticketList)


def parse_ages():
    parse = argparse.ArgumentParser(description=u'search_index update')
    parse.add_argument(u'--mode', default=u'', help=u'[hotel] or [ticket]')
    parse.add_argument(u'--t', default=u'test', help=u'[test]')
    return parse.parse_args()


if __name__ == '__main__':
    ages = parse_ages()
    if ages.t == 'test':
        kafkaHost = '172.18.5.4:9092'  # 测试
        print '测试'
    else:
        kafkaHost = '172.18.100.44:9092'  # 正式
        print '正式'
    if ages.mode == u'hotel':
        hotelPro(kafkaHost)
    elif ages.mode == u'ticket':
        #index = 'alias_cn_yyn_ticket'
        index = 'idx_cn_yyn_ticket_20211230'
        ticketPro(kafkaHost, index)
    elif ages.mode == u'scenic':
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_scenic_pop(producer)
    elif ages.mode == u'shopping':
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_shopping(producer)
    elif ages.mode == u'cloud9_shopping':
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_cloud9_shopping(producer)
    elif ages.mode == u'video':     # 更新视频的景区等级
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_video(producer)
    elif ages.mode == u'live':      # 更新直播的景区等级，和景观类型
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_live(producer)
    elif ages.mode == u'en_Activities':     # 从伟志的接口更新英文版搜索的活动
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_en_Activities(producer)
    elif ages.mode == u'traffic':   #交通出行   门票拿数据
        producer = KafkaProducer(bootstrap_servers=kafkaHost)
        update_index_traffic(producer)
    elif age.mode == u'coupon':     #团购精选   酒店，诚选，门票，线路
        pass 
